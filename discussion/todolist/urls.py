from django.urls import path
from . import views

urlpatterns = [
	path('', views.index, name="index"),
	# localhost:8000/todolist/1
	path('<int:todoitem_id>/', views.todoitem, name="viewtodoitem"),
	path('register/', views.register, name="register"),
	path('change-password/', views.change_password, name="change_password"),
	path('login/', views.login_view, name="login"),
	path('logout/', views.logout_view, name="logout")
]	
